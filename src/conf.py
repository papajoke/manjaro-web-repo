import sys

MIRRORS_URL = "https://repo.manjaro.org/mirrors.json"
BRANCHES = ("stable", "testing", "unstable")
ROOT_FOLDER = "/var/www/manjaro-web-repo/"
HASH_FOLDER = "/var/repo/repo/"
OUTPUT_FOLDER = "docs/"
LOGS_FOLDER = "logs/"
IS_DEV = False
JSON_FILE = "mirrors.json"

if '--dev' in sys.argv:
    IS_DEV = True
    ROOT_FOLDER = "./var/www/manjaro-web-repo/"
    HASH_FOLDER = "./var/repo/repo/"
    if len(sys.argv) > 2:
        JSON_FILE = sys.argv[2]
    print("# DEV mode, use local folders and local json source, ignore hashes")
